
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;







public class BoardArray extends javax.swing.JFrame implements ActionListener{

/**
 * Creates new form BoardArray
 */
public BoardArray() {
    initComponents();
    initTableButtoms();
    newGame();
    board.getCurrent();
    board.getPlayer();
    
    showBoard();
}

@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Table1 = new javax.swing.JButton();
        Table2 = new javax.swing.JButton();
        Table3 = new javax.swing.JButton();
        Table4 = new javax.swing.JButton();
        Table5 = new javax.swing.JButton();
        Table6 = new javax.swing.JButton();
        Table7 = new javax.swing.JButton();
        Table8 = new javax.swing.JButton();
        Table9 = new javax.swing.JButton();
        outputText = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Table1.setFont(new java.awt.Font("TH Baijam", 1, 80)); // NOI18N
        Table1.setText("1");
        Table1.setActionCommand("0");
        Table1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        Table1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Table1.setMaximumSize(new java.awt.Dimension(50, 50));
        Table1.setMinimumSize(new java.awt.Dimension(50, 50));

        Table2.setFont(new java.awt.Font("TH Baijam", 1, 80)); // NOI18N
        Table2.setText("2");
        Table2.setActionCommand("1");
        Table2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Table2.setMaximumSize(new java.awt.Dimension(50, 50));
        Table2.setMinimumSize(new java.awt.Dimension(50, 50));

        Table3.setFont(new java.awt.Font("TH Baijam", 1, 80)); // NOI18N
        Table3.setText("3");
        Table3.setActionCommand("2");
        Table3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Table3.setMaximumSize(new java.awt.Dimension(50, 50));
        Table3.setMinimumSize(new java.awt.Dimension(50, 50));

        Table4.setFont(new java.awt.Font("TH Baijam", 1, 80)); // NOI18N
        Table4.setText("4");
        Table4.setActionCommand("3");
        Table4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Table4.setMaximumSize(new java.awt.Dimension(50, 50));
        Table4.setMinimumSize(new java.awt.Dimension(50, 50));

        Table5.setFont(new java.awt.Font("TH Baijam", 1, 80)); // NOI18N
        Table5.setText("5");
        Table5.setActionCommand("4");
        Table5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Table5.setMaximumSize(new java.awt.Dimension(50, 50));
        Table5.setMinimumSize(new java.awt.Dimension(50, 50));

        Table6.setFont(new java.awt.Font("TH Baijam", 1, 80)); // NOI18N
        Table6.setText("6");
        Table6.setActionCommand("5");
        Table6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Table6.setMaximumSize(new java.awt.Dimension(50, 50));
        Table6.setMinimumSize(new java.awt.Dimension(50, 50));

        Table7.setFont(new java.awt.Font("TH Baijam", 1, 80)); // NOI18N
        Table7.setText("7");
        Table7.setActionCommand("6");
        Table7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Table7.setMaximumSize(new java.awt.Dimension(50, 50));
        Table7.setMinimumSize(new java.awt.Dimension(50, 50));

        Table8.setFont(new java.awt.Font("TH Baijam", 1, 80)); // NOI18N
        Table8.setText("8");
        Table8.setActionCommand("7");
        Table8.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Table8.setMaximumSize(new java.awt.Dimension(50, 50));
        Table8.setMinimumSize(new java.awt.Dimension(50, 50));

        Table9.setFont(new java.awt.Font("TH Baijam", 1, 80)); // NOI18N
        Table9.setText("9");
        Table9.setActionCommand("8");
        Table9.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        Table9.setMaximumSize(new java.awt.Dimension(50, 50));
        Table9.setMinimumSize(new java.awt.Dimension(50, 50));

        outputText.setFont(new java.awt.Font("TH Chakra Petch", 0, 36)); // NOI18N
        outputText.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        outputText.setText("Result :");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(47, 47, 47)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(outputText, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Table7, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(Table8, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(Table9, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Table4, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(Table5, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(Table6, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Table1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(Table2, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(Table3, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(56, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(114, 114, 114)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Table1, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Table2, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Table3, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Table4, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Table6, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Table5, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Table7, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Table9, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Table8, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(31, 31, 31)
                .addComponent(outputText, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(84, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    
    
    
    
public static void main(String args[]) {
    /* Set the Nimbus look and feel */
    //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
     */
    try {
        for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
            if ("Nimbus".equals(info.getName())) {
                javax.swing.UIManager.setLookAndFeel(info.getClassName());
                break;
            }
        }
    } catch (ClassNotFoundException ex) {
        java.util.logging.Logger.getLogger(BoardArray.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (InstantiationException ex) {
        java.util.logging.Logger.getLogger(BoardArray.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (IllegalAccessException ex) {
        java.util.logging.Logger.getLogger(BoardArray.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    } catch (javax.swing.UnsupportedLookAndFeelException ex) {
        java.util.logging.Logger.getLogger(BoardArray.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
    }
    //</editor-fold>

    /* Create and display the form */
    java.awt.EventQueue.invokeLater(new Runnable() {
    public void run() {
        new BoardArray().setVisible(true);
    }
    });
}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Table1;
    private javax.swing.JButton Table2;
    private javax.swing.JButton Table3;
    private javax.swing.JButton Table4;
    private javax.swing.JButton Table5;
    private javax.swing.JButton Table6;
    private javax.swing.JButton Table7;
    private javax.swing.JButton Table8;
    private javax.swing.JButton Table9;
    private javax.swing.JLabel outputText;
    // End of variables declaration//GEN-END:variables
    private javax.swing.JButton[][] tableButtoms;
    private Player X = new Player('X');
    private Player O= new Player('O');
    private Board board;
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String indexString = e.getActionCommand();
        int index = Integer.parseInt(indexString);
        int row = index/3;
        int col = index%3;
        outputText.setText("Row = "+ row+ " , Col = "+ col);
         board.setTable(row,col);
         showBoard();
    }

    private void initTableButtoms() {
       javax.swing.JButton[][] tableButtoms ={
           {Table1,Table2,Table3},
           {Table4,Table5,Table6},
           {Table7,Table8,Table9}
        };
       this.tableButtoms = tableButtoms;
       for (int row =0 ; row < tableButtoms.length ; row++){
           for(int col=0 ; col < tableButtoms[row].length ; col++){
               tableButtoms[row][col].addActionListener(this);
           }
       }
    }

    private void newGame() {
       board = new  Board(X,O);
    }

    private void showBoard() {
          char[][] table = board.getTable();
           for (int row =0 ; row < tableButtoms.length ; row++){
           for(int col=0 ; col < tableButtoms[row].length ; col++){
               tableButtoms[row][col].setText(" " +board.Table[row][col]);
           }
       }
    }
}
