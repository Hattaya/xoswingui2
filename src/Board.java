
public class Board {
	private Player x;
	private Player o;
	private Player winner;
	private Player current;
	private int player;
	private int turnCount;
	private int row ,column,result;
	static char[][] Table = {
			{'1','2','3'},
			{'4','5','6'},
			{'7','8','9'}};
	
	Board(Player x, Player o){
		this.x = x;
		this.o = o;
		current = x ;
		player = 1;
		turnCount = 0;
		winner = null;
	}
	public int getCount() {
		turnCount+=1;
		return turnCount;
	}
	public int getResult() {
		return result;
	}
	
	public boolean checkWin() {
		if( (Table[0][0] == Table [1][1] && Table[0][0] == Table [2][2]) ||
				Table[0][2] == Table [1][1] && Table[0][2] == Table [2][0]) {
				winner = current;
				if(current == x) {
					x.win();
					o.lose();
				}else {
					o.win();
					x.lose();
				}
				return true;
			}else {
				for(int line=0 ; line <=2 ; line++) {
					if((Table[line][0] == Table[line][1] && Table[line][0] == Table[line][2])||
							(Table[0][line] == Table[1][line] && Table[0][line] == Table[2][line])) {
						winner = current;
						if(current == x) {
							x.win();
							o.lose();
						}else {
							o.win();
							x.lose();
						}
						System.out.println("Congratuiations!!, player" +player+"["+current+"]"+", YOU ARE THE WINNER!");
						return true;
					}
				}
			}
			return false;
	}
	
	public boolean checkDraw() {
		if(getCount() >=9) {
			x.draw();
			o.draw();
			return true;
		}
		return false;	
	}
	
	public boolean isEnd() {
		if(checkWin()) {
			result=1;
			return true;
		}
		if(checkDraw()) {
			result=2;
			return true;
		}
		return false;
		
	}
        
	public char[][] getTable(){
                         return Table;
            
        }
	
	public int getPlayer() {
		return player;
	}
	
	public Player getCurrent() {
		return current;
		
	}
	public void swichTurn() {
		if(player==2) {
			player--;
			current =x;
		}else {
			player++;
			current = o;
		}
	}
	public int getRow(int go) {
		row = --go/3;
		return  row;
	}
	public int getColumn(int go) {
		column = --go%3;
		return  column;
	}
	public Player getWinner() {
		return winner;
	}

                    public boolean setTable(int row, int col) {
                        if(Table[row][col] =='1'  || Table[row][col]  =='2'|| Table[row][col]  =='3'
                            || Table[row][col]  =='4'|| Table[row][col]  =='5'|| Table[row][col]  =='6'
                            || Table[row][col] =='7'|| Table[row][col]  =='8'|| Table[row][col] =='9') {
                            Table[row][col]  = getCurrent().getName();
                            return true;
	       }
                        return false;
	}
    }
	
	
